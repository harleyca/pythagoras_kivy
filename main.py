import math

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout

# creating the root widget used in .kv file
class BoxLayout(BoxLayout):
    def btn(self, instance, value):
        c = int(math.sqrt(int(self.a.text)**2 + int(self.b.text)**2))
        self.c.text = ("Hypotenuse: " + str(c))


class MyApp(App):
    def build(self):
        #Window.size = (1080, 1920)
        self.title = 'Pythagoras'
        return BoxLayout()

if __name__ == '__main__':
    MyApp().run()


