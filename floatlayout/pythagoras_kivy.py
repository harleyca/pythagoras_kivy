import math

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.floatlayout import FloatLayout

# creating the root widget used in .kv file
class FloatLayout(FloatLayout):
    def btn(self, instance, value):
        c = int(math.sqrt(int(self.a.text)**2 + int(self.b.text)**2))
        self.c.text = ("Hypotenuse: " + str(c))


class MyApp(App):
    def build(self):
        Window.size = (600,400)
        self.title = 'Pythagoras'
        return FloatLayout()

if __name__ == '__main__':
    MyApp().run()


